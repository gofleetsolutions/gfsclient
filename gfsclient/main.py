# -*- coding: utf-8 -*-
# Copyright 2018-2019 GO FLEET SOLUTIONS S.R.L. See LICENSE for details.
"""
gfsclient entry point
"""

import argparse
import os
import sys
import json
from threading import Thread

import zmq


from . import __version__
from .livegraph import LiveGraph

VERSION_INTRO = r'gfsclient v{}'.format(__version__)


def determine_path():
    try:
        root = __file__
        if os.path.islink(root):
            root = os.path.realpath(root)
        return os.path.dirname(os.path.abspath(root))

    except:
        print("Invalid __file__ variable.")
        sys.exit()


def parse_args():
    parser = argparse.ArgumentParser(description='Listen events from target.')
    parser.add_argument('target', help='target address')
    parser.add_argument('-v', '--version', dest='show_version', action='store_true')

    return vars(parser.parse_args())


class ReceiverStream:
    """ stream capture in a separate thread
    """
    def __init__(self, receiver):
        self.data = []
        self.stopped = False
        self.receiver = receiver

    def start(self):
        """ Start the thread to read data from receiver
        """
        self.stopped = False
        Thread(target=self.update, args=()).start()
        return self

    def update(self):
        """ Loop indefinitely until the thread is stopped
        """
        while not self.stopped:
            message = self.receiver.recv_multipart()
            message = json.loads(message[1].decode("utf-8"))
            count = message['count']
            self.data.append(count)

    def read(self):
        """ Return the frame most recently read
        """
        return self.data

    def clear(self):
        """ Clear the data stream
        """
        self.data = []

    def stop(self):
        """ Set the flag that indicate the thread should be stopped
        """
        self.stopped = True


def main():
    arguments = parse_args()
    print(arguments)
    if arguments.get('show_version'):
        print(VERSION_INTRO)
        return

    context = zmq.Context()
    subscriber = context.socket(zmq.SUB)
    target = arguments['target']
    subscriber.connect(target)

    subscriber.setsockopt_string(zmq.SUBSCRIBE, 'SCAN')
    stream = ReceiverStream(subscriber)
    graph = LiveGraph(stream)

    stream.start()
    graph.start()

    stream.stop()


if __name__ == '__main__':
    main()
