# -*- coding: utf-8 -*-
# Copyright 2018-2019 GO FLEET SOLUTIONS S.R.L. See LICENSE for details.
"""
livegraph code responsible to create the livegraph
"""

import datetime as dt
import matplotlib.pyplot as plt
import matplotlib.animation as animation

import queue

# Create figure for plotting
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
xs = []
ys = []

data = []

# This function is called periodically from FuncAnimation
def animate(i, stream, xs, ys):
    data = stream.read()
    number = max(data) if data else 0
    stream.clear()

    # Add x and y to lists
    xs.append(dt.datetime.now().strftime('%H:%M:%S.%f'))
    ys.append(number)

    # Limit x and y lists to 20 items
    xs = xs[-20:]
    ys = ys[-20:]

    # Draw x and y lists
    ax.clear()
    ax.plot(xs, ys)

    # Format plot
    plt.xticks(rotation=45, ha='right')
    plt.subplots_adjust(bottom=0.30)
    plt.title('Number of detected objects')


class LiveGraph:
    def __init__(self, stream):
        self.stream = stream
        self.annimation = animation.FuncAnimation(fig, animate, fargs=(self.stream, xs, ys), interval=1000)

    def start(self):
        plt.show()
