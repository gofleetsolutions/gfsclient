# -*- coding: utf-8 -*-
# Copyright 2018-2019 GO FLEET SOLUTIONS S.R.L. See LICENSE for details.
"""
gfsclient setup and installation script.
"""

import io
import os
import re

try:
    from setuptools import setup, find_packages
except ImportError:
    from distutils.core import setup, find_packages


def get_version():
    """Read and return version number"""
    version_file = os.path.join('gfsclient', '__init__.py')
    version_file_lines = open(version_file, 'rt').readlines()
    version_re = r"^__version__ = ['\"]([^'\"]*)['\"]"
    for line in version_file_lines:
        match_obj = re.search(version_re, line, re.M)
        if match_obj:
            return match_obj.group(1)
    raise RuntimeError('Unable to find version string in {}.'.format(version_file))


def read(*names, **kwargs):
    """Read a file."""
    return io.open(
        os.path.join(os.path.dirname(__file__), *names),
        encoding=kwargs.get('encoding', 'utf8')
    ).read().strip()


test_deps = [
    'flake8',
    'autoflake',
    'coverage',
    'epc',
    'jedi',
    'pycodestyle',
    'pyflakes',
    'pylint',
    'pylint-json2html',
    'pytest',
    'pytest-html',
    'yapf',
]

extras = {
    'test': test_deps,
}

setup(name='gfsclient',
      version=get_version(),
      url='https://gitlab.com/gofleetsolutions/gfsclient',
      license='MIT',
      author='Remi Doumenc',
      author_email='remi.doumenc@gmail.com',
      description='GFS client',
      long_description=read("README.md"),
      long_description_content_type='text/markdown',
      entry_points={
          'console_scripts': [
              'gfsclient = gfsclient.main:main',
          ],
      },
      packages=find_packages(exclude=['tests']),
      include_package_data=True,
      zip_safe=False,
      platforms='any',
      install_requires=[
          'pyzmq',
          'matplotlib',
      ],
      test_suite='tests',
      tests_require=test_deps,
      extras_require=extras,
      setup_requires=['setuptools>=38.6.0'],
      classifiers=[
          'Development Status :: 3 - Alpha'
          'Intended Audience :: Developers',
          'License :: OSI Approved :: MIT License',
          'Natural Language :: English',
          'Operating System :: OS Independent',
          'Programming Language :: Python',
          'Programming Language :: Python :: 3',
          'Programming Language :: Python :: 3 :: Only',
          'Programming Language :: Python :: 3',
          'Programming Language :: Python :: 3.4',
          'Programming Language :: Python :: 3.5',
          'Programming Language :: Python :: 3.6',
          'Programming Language :: Python :: 3.7',
          'Topic :: Software Development :: Libraries',
          'Topic :: Software Development :: Libraries :: Python Modules',
          'Topic :: Utilities',
      ])
