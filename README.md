# GFS Client
GFSClient is a Python3 project using [ZeroMQ](http://zeromq.org/) to collect data from GFS target and plot data.

# Installation

Clone the source tree:
```sh
git clone https://gitlab.com/gofleetsolutions/gfsclient.git
```

Install gfsclient with:
```sh
python3 setup.py install
```

# Usage
From the command line, listen events from target:
```sh
gfsclient 'tcp://192.168.178.1:55555'
```

# Reports

* [Pylint report](https://gofleetsolutions.gitlab.io/gfsclient/quality-report/pylint-report.html)
* [Coverage report](https://gofleetsolutions.gitlab.io/gfsclient/coverage-report/index.html)
* [Unit tests report](https://gofleetsolutions.gitlab.io/gfsclient/test-report/test_report.html)
